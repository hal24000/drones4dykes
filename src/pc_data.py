"""
Includes functions and classes used for processing the point cloud data
"""
from __future__ import annotations
import laspy
import numpy as np
import scipy.ndimage as ndimage
import scipy.interpolate as interp

# Constants
COLORMAX = 65535
EARTH_RADIUS = 6378e3

class GridTool:
    """
    Interpolates a point cloud onto a 2D surface, where
    x and y are a uniform grid, while elevation and colors
    are defined as functions of (x,y)/(lon, lat).
    
    Parameters
    ----------
    point_cloud
        LAS point cloud (previously open by laspy)
    nx, ny
        Number of points in the horizontal grid
    skip
        If different from None, the point cloud is downsampled taking 1 
        out every `skip` points
    method
        The method used by `scipy.interpolate.griddata` to do the 
        interpolation.
        
    """
    def __init__(self, 
        point_cloud : laspy.lasdata.LasData,
        nx : int,
        ny : int,
        skip : int | None = 2,
        method : Literal['linear', 'nearest', 'cubic'] = 'nearest',
        color_max : int = 65535
    ):
        self.point_cloud = point_cloud
        self.nx, self.ny = nx, ny
        self.skip = skip
        self.method = method
                
        # Initializes place-holders
        for attr in ('lon', 'lat', 'x', 'y', 'z', 'color'):
            setattr(self, f'_{attr}', None)
        
    def _construct_grid(self):
        x, y = (
            np.linspace(
                getattr(self.point_cloud,coord).min(),
                getattr(self.point_cloud,coord).max(),
                getattr(self,'n'+coord)
            ) for coord in ('x','y')
        )        
        self._lon, self._lat = np.meshgrid(x, y)
        
    @property 
    def lon(self):
        """Uniform grid of longitude values"""
        if self._lon is None:
            self._construct_grid()
        return self._lon
    
    
    @property 
    def lat(self):
        """Uniform grid of latitude values"""
        if self._lat is None:
            self._construct_grid()
        return self._lat
    
    @property 
    def x(self):
        """Uniform grid of longitude values in meters"""
        if self._x is None:
            mid_lon, _ = self.coords_centre
            self._x = np.deg2rad(self.lon - mid_lon)*EARTH_RADIUS
        return self._x
    
    @property 
    def y(self):
        """Uniform grid of latitude values in meters"""
        if self._y is None:
            _, mid_lat = self.coords_centre
            self._y = np.deg2rad(self.lat - mid_lat)*EARTH_RADIUS
        return self._y
    
        
    @property
    def z(self):
        """Elevation"""
        if self._z is None:
            self._z = self._grid_quantity(self.point_cloud.z)
        return self._z
    
    @property
    def color(self):
        """Grid of RGB color data (scaled to the [0,1] interval"""
        if self._color is None:
            colors = [
                self._grid_quantity(self.point_cloud[color])
                for color in ('red', 'green', 'blue')
            ]
            self._color = np.stack(colors,axis=-1)/COLORMAX

        return self._color
            
    def _grid_quantity(self, quantity):
        """Creats an interpolated grid from a quantity"""
        select = slice(None, None, self.skip)
        return interp.griddata(
            (self.point_cloud.x[select], self.point_cloud.y[select]), 
            quantity[select], (self.lon, self.lat), 
            method=self.method
        )
    
    @property
    def extent_m(self):
        """Returns the "extent" in meters in a matplotlib's `imshow` compatible way"""
        return (self.x[0,0], self.x[-1,-1], self.y[0,0], self.y[-1,-1])
    
    @property
    def extent_coord(self):
        """Returns the "extent" in degrees in a matplotlib's `imshow` compatible way"""
        return (self.lon[0,0], self.lon[-1,-1], self.lat[0,0], self.lat[-1,-1])
    
    @property
    def coords_centre(self):
        """Coordinate value of the centre of the grid"""
        return np.median(self.lon), np.median(self.lat)