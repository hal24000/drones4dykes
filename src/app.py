import streamlit as st
import laspy as lp
import numpy as np
import plotly.express as px
import pandas as pd
from matplotlib import colors
from typing import Literal

COLORMAX = 65535
EARTH_RADIUS = 6378e3 # m


point_cloud = lp.read('../data/DJI L1 proces bestanden/lidars/terra_las/cloud_-35fa9ee8.las')


def prepare_data(point_cloud,
                 skip : int = 20000, ax = None, 
                 coords_type : Literal['coords','meters','delta'] = 'meters',
                 delta_coords=False, **kwargs):

    # Converts the RGB data into a downsampled (N,3) array
    # # whose values are within [0,1]
    colors = [
        f'rgb({r*255//COLORMAX},{g*255//COLORMAX},{b*255//COLORMAX})'
        for r,g, b in zip(point_cloud.red[::skip],
                          point_cloud.green[::skip],
                          point_cloud.blue[::skip])
    ]
    
    if coords_type in ('meters', 'delta'):
        x_mid = np.median(point_cloud.x)
        y_mid = np.median(point_cloud.y)
    else:
        x_mid = y_mid = 0
        
    
    x = np.array(point_cloud.x[::skip] - x_mid)
    y = np.array(point_cloud.y[::skip] - y_mid)
    
    if coords_type == 'meters':
        x = np.deg2rad(x)*EARTH_RADIUS
        y = np.deg2rad(y)*EARTH_RADIUS
    
    z = np.array(point_cloud.z[::skip])
    
    data={'x': x, 'y': y, 'z': z, 'color': colors}
    return data

def show_point_cloud(point_cloud,
                     skip : int = 1500, ax = None, delta_coords=False,
                     **kwargs):
    data = prepare_data(point_cloud)
    
    fig = px.scatter_3d(x=data['x'],y=data['y'],z=data['z'])
    
    fig.data[0].marker.color = data['color']
    fig.data[0].marker.size = 1
    
    return fig


st.set_page_config(layout="wide", page_title="Dyke inspection helper")

st.write('hello')

fig = show_point_cloud(point_cloud)
st.plotly_chart(fig)
# skip=20000
# # COLORMAX = 65535
# x = [1,2,3]
# y = [1,2,3]
# z = [1,2,3]
# fig = px.scatter_3d(x=x,y=y,z=z)

# fig.data[0].marker.color = ['red','blue','green']
# fig.data[0].marker.size = 1
# st.plotly_chart(fig)



# import plotly.graph_objects as go
# from plotly.subplots import make_subplots
# import numpy as np

# N = 20
# x = np.linspace(0, 1, N)

# fig = make_subplots(1, 3)
# for i in range(1, 4):
#     fig.add_trace(px.imshow(img_rgb).data[0], 1, i)

# fig.update_xaxes(matches='x')
# fig.update_yaxes(matches='y')
# fig.show()